
package cat.itb.practica3_formulari;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity3 extends AppCompatActivity {

    private Button btnShow;
    private Button btnShare;

    //main 1
    private String name;

    //main2
    private int age;
    private String holaAdeu; //true = hola
    private String finalMessage;
    /*
    “Hola Dani, com portes aquests 25 anys?”
    o “Espero tornar a veure’t Dani, abans que facis 26 anys”
    * */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        btnShow = findViewById(R.id.btnShow);
        btnShare = findViewById(R.id.btnShare);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.name = bundle.getString("name");
            this.age = bundle.getInt("age");
            this.holaAdeu = bundle.getString("holaAdeu");
            if (Boolean.parseBoolean(this.holaAdeu))
                finalMessage = "Hola " + this.name + ", com portes aquests " + this.age + " anys?";
            else{
                finalMessage = "Espero tornar a veure't " + this.name + ", abans de que facis " + (this.age + 1) + " anys.";
            }
        } else {
            Toast.makeText(MainActivity3.this, "No se recibieron datos!", Toast.LENGTH_SHORT).show();
        }

        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity3.this, finalMessage, Toast.LENGTH_LONG).show();
            }
        });

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                btnShow.setVisibility(View.INVISIBLE);
            }
        });


    }
}

