package cat.itb.practica3_formulari;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText editTextname;
    private Button btnNextActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Mostrar icono en ActionBar
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        editTextname = findViewById(R.id.inputName);
        btnNextActivity = findViewById(R.id.btnNextActivity);

        btnNextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*String name = editTextname.getText().toString();
                Toast.makeText(MainActivity.this, "Hi "+name + " ¡Click next activity!", Toast.LENGTH_SHORT).show();*/
                if (!editTextname.getText().toString().isEmpty()){
                    String name = editTextname.getText().toString();
                    Toast.makeText(MainActivity.this, "Hi "+name + " ¡Click next activity!", Toast.LENGTH_SHORT).show();

                    //try intent
                    Intent intent = new Intent (MainActivity.this, MainActivity2.class);
                    intent.putExtra("name", name);
                    startActivity(intent);


                } else {
                    Toast.makeText(MainActivity.this, "Need your name for go to next activity", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }
}