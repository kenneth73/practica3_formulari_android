



package cat.itb.practica3_formulari;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {

    private Button btnNextActivity;
    private RadioButton radioButtonHola;
    private RadioButton radioButtonAdeu;

    private SeekBar seekBar;
    private TextView textViewNumber;

    //main 1
    private String name;

    //main2
    private int age;
    private boolean holaAdeu; //true = hola



    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //Fletxa per tornar enrere
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        btnNextActivity = findViewById(R.id.btnNextActivity);
        radioButtonHola= findViewById(R.id.hola);
        radioButtonAdeu= findViewById(R.id.adeu);
        seekBar= findViewById(R.id.seekBar);
        textViewNumber= findViewById(R.id.numberSeekBar);
        textViewNumber.setText("18");
        seekBar.setProgress(18);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.name = bundle.getString("name");
            Toast.makeText(MainActivity2.this, "Recibimos los datos! Nombre: " + name, Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(MainActivity2.this, "No se recibieron datos!", Toast.LENGTH_SHORT).show();
        }

        setSeekBar();

        btnNextActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TODO: hacer en intent
                if (age >= 18 && age <= 60){
                    Toast.makeText(MainActivity2.this, "Go to next activity!", Toast.LENGTH_SHORT).show();
                    //try intent

                    if (radioButtonHola.isChecked()){
                        holaAdeu = true;
                    } else {
                        holaAdeu = false;
                    }


                    Intent intent = new Intent (MainActivity2.this, MainActivity3.class);
                    intent.putExtra("name", name);
                    intent.putExtra("age", age);
                    intent.putExtra("holaAdeu", holaAdeu);
                    startActivity(intent);


                } else {
                    Toast.makeText(MainActivity2.this, "Can't go to next activity!(you have to be of legal age)", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
    public void setSeekBar() {
        //textViewNumber.setText(seekBar.getProgress());
        seekBar.setMax(90);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                age = progress;
                String progressStr = String.valueOf(progress);
                textViewNumber.setText(progressStr);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() < 18 || seekBar.getProgress() > 60){
                    btnNextActivity.setVisibility(View.INVISIBLE);
                    Toast.makeText(MainActivity2.this, "Solo para personas entre 18 y 60 años", Toast.LENGTH_SHORT).show();

                } else {
                    btnNextActivity.setVisibility(View.VISIBLE);
                }
                MainActivity2.this.age = seekBar.getProgress();
            }
        });
    }

}